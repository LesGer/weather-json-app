package weatherApp.logic.listeners;

import org.junit.Test;
import weatherApp.gui.GUIItems;
import weatherApp.model.Config;

import static org.junit.Assert.*;

public class GetWeatherListenerTest {

    @Test
    public void wind_10(){

        int windDir = 10;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("North",windDirection);
    }

    @Test
    public void wind_330(){

        int windDir = 330;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("North",windDirection);
    }

    @Test
    public void wind_31(){

        int windDir = 31;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("North-East",windDirection);
    }

    @Test
    public void wind_74(){

        int windDir = 74;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("North-East",windDirection);
    }
    @Test
    public void wind_75(){

        int windDir = 75;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("East",windDirection);
    }
    @Test
    public void wind_114(){

        int windDir = 114;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("East",windDirection);
    }
    @Test
    public void wind_115(){

        int windDir = 115;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("South-East",windDirection);
    }
    @Test
    public void wind_149(){

        int windDir = 149;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("South-East",windDirection);
    }
    @Test
    public void wind_150(){

        int windDir = 150;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("South",windDirection);
    }
    @Test
    public void wind_194(){

        int windDir = 194;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("South",windDirection);
    }
    @Test
    public void wind_195(){

        int windDir = 195;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("South-West",windDirection);
    }
    @Test
    public void wind_239(){

        int windDir = 239;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("South-West",windDirection);
    }
    @Test
    public void wind_240(){

        int windDir = 240;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("West",windDirection);
    }
    @Test
    public void wind_284(){

        int windDir = 284;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("West",windDirection);
    }
    @Test
    public void wind_285(){

        int windDir = 285;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("North-West",windDirection);
    }
    @Test
    public void wind_329(){

        int windDir = 329;

        String windDirection = new GetWeatherListener(new Config(), new GUIItems()).windDirection(windDir);

        assertEquals("North-West",windDirection);
    }


}