package weatherApp.gui;

import weatherApp.logic.listeners.ListenerFactory;
import weatherApp.model.Config;

import javax.swing.*;
import java.awt.*;

public class KeyWindowGUI {

    private JFrame mainWindow;
    private JPanel mainPanel;
    private JPasswordField apiKey;
    private Config config;

    public KeyWindowGUI(){
        this.mainWindow = new JFrame("Enter API key");
        this.mainPanel = new JPanel();
        this.apiKey = new JPasswordField();
        this.config = new Config();
        frameIni();
        paneIni();
    }

    private void frameIni(){
        mainWindow.setPreferredSize(new Dimension(400,150));
        mainWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainWindow.pack();
        mainWindow.setVisible(true);
    }

    private void paneIni(){
        mainWindow.add(mainPanel);
        mainPanel.add(new Label("Enter API key"));
        mainPanel.add(apiKey = passwordFieldIni());
        mainPanel.add(buttonIni("OK","ok"));
        mainPanel.add(buttonIni("How to get your API key","help"));

    }

    private JPasswordField passwordFieldIni(){
        JPasswordField passwordField = new JPasswordField(20);
        passwordField.addActionListener(ListenerFactory.factory("ok", mainWindow, apiKey, config));
        return passwordField;
    }

    private JButton buttonIni(String text, String command){
        JButton button = new JButton(text);
        button.addActionListener(ListenerFactory.factory(command, mainWindow, apiKey, config));
        button.setActionCommand(command);
        return button;
    }


    public JFrame getMainWindow() {
        return mainWindow;
    }

    public void setMainWindow(JFrame mainWindow) {
        this.mainWindow = mainWindow;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public void setMainPanel(JPanel mainPanel) {
        this.mainPanel = mainPanel;
    }

    public JPasswordField getApiKey() {
        return apiKey;
    }

    public void setApiKey(JPasswordField apiKey) {
        this.apiKey = apiKey;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }
}
