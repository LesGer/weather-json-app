package weatherApp.gui;

import weatherApp.logic.listeners.AboutListener;
import weatherApp.logic.listeners.ListenerFactory;
import weatherApp.model.Config;

import javax.swing.*;
import java.awt.*;

import java.awt.event.KeyEvent;

public class MainWindowGUI {

    private Config config;
    private JPanel mainPane;
//    private JComboBox<String> country;
    private GUIItems guiItems;
//    private ReadCountriesFromServer readCC;

    public MainWindowGUI(Config config) {
        this.config = config;
        this.guiItems = new GUIItems();
//        this.readCC = new ReadCountriesFromServer();
        this.mainPane = mainPaneIni();
//        this.country = new JComboBox<>();

        frameInitialization();
        labelInit();
        textIni();
//        comboBoxIni();
        buttonIni();
    }

    private void frameInitialization() {
        guiItems.getFrame().setTitle("Weather App");
        guiItems.getFrame().add(mainPane);
        guiItems.getFrame().setPreferredSize(guiItems.getDimension());
        guiItems.getFrame().setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        guiItems.getFrame().setResizable(false);
        guiItems.getFrame().setJMenuBar(new MainWindowMenuBar());
        guiItems.getFrame().pack();
        guiItems.getFrame().setVisible(true);
    }

    private JPanel mainPaneIni() {
        JPanel main = new JPanel();
        main.setLayout(null);
        return main;
    }

//    private void comboBoxIni() {
//        mainPane.add(country = windowCombo(225, 15));
//        for (Map.Entry<String, String> entry : readCC.getCountriesAndCapitals().entrySet()) {
//            country.addItem(entry.getKey());
//        }
//
//    }

    private void buttonIni() {
        mainPane.add(windowButton(225, 15,  "Get Weather", "get"));
        mainPane.add(windowButton(225, 125,  "More info", "expand"));
    }

    private void textIni() {
        guiItems.setCityText(cityTextField());
        mainPane.add(guiItems.getCityText());
    }

    private void labelInit() {
        setWeatherInfo();
        setWeatherInfoIcons();
        addLabelsToPane();
    }

    private void addLabelsToPane() {
        mainPane.add(windowLabel(15, 15, "City"));
        mainPane.add(guiItems.getWeatherIcon());
        for (JLabel infoLabel : guiItems.getWeatherInfo()) {
            mainPane.add(infoLabel);
        }
        for (JLabel weatherInfoLabel : guiItems.getWeatherInfoIcons()) {
            mainPane.add(weatherInfoLabel);
        }
    }

    private void setWeatherInfoIcons() {
        guiItems.setWeatherIcon(weatherIconLabel());

        guiItems.getWeatherInfoIcons()[0] = smallWeatherIconLabel(15, 180);
        guiItems.getWeatherInfoIcons()[1] = smallWeatherIconLabel(215, 180);
        guiItems.getWeatherInfoIcons()[2] = smallWeatherIconLabel(15, 250);
        guiItems.getWeatherInfoIcons()[3] = smallWeatherIconLabel(215, 250);
        guiItems.getWeatherInfoIcons()[4] = smallWeatherIconLabel(15, 320);
        guiItems.getWeatherInfoIcons()[5] = smallWeatherIconLabel(215, 320);
    }

    private void setWeatherInfo() {
        guiItems.getWeatherInfo()[0] = windowLabel(140, 50, 45, setFont(30), Color.BLACK);
        guiItems.getWeatherInfo()[1] = windowLabel(100, 100, "");
        guiItems.getWeatherInfo()[2] = windowLabel(75, 180, 50, setFont(15), Color.RED);
        guiItems.getWeatherInfo()[3] = windowLabel(270, 180, 50, setFont(15), Color.BLUE);
        guiItems.getWeatherInfo()[4] = windowLabel(75, 250, 50, setFont(15), Color.BLACK);
        guiItems.getWeatherInfo()[5] = windowLabel(270, 250, 50, setFont(15), Color.BLACK);
        guiItems.getWeatherInfo()[6] = windowLabel(75, 320, 50, setFont(15), Color.BLACK);
        guiItems.getWeatherInfo()[7] = windowLabel(270, 320, 50, setFont(15), Color.BLACK);
    }

    private Font setFont(int size) {
        return new Font("Arial", Font.PLAIN, size);
    }

    private JButton windowButton(int x, int y, String text, String command) {
        JButton button = new JButton(text);
        button.setBounds(x, y, 150, 25);
        button.setActionCommand(command);
        button.addActionListener(ListenerFactory.factory(command, config, guiItems));
        return button;
    }


//    private JComboBox<String> windowCombo(int x, int y) {
//        JComboBox<String> combo = new JComboBox<>();
//        combo.setBounds(x, y, 200, 25);
//        combo.setActionCommand("combo");
//        combo.addItemListener(ListenerFactory.factory("combo", city,
//                readCC.getCountriesAndCapitals(), guiItems.getCityText()));
//        return combo;
//
//    }

    private JLabel windowLabel(int x, int y, String text) {
        JLabel label = new JLabel(text);
        label.setBounds(x, y, 250, 25);
        label.setFont(setFont(20));
        return label;
    }

    private JLabel windowLabel(int x, int y, int height, Font font, Color color) {
        JLabel label = new JLabel("");
        label.setBounds(x, y, 140, height);
        label.setFont(font);
        label.setOpaque(true);
        label.setForeground(color);
        return label;
    }

    private JLabel smallWeatherIconLabel(int x, int y) {
        JLabel label = new JLabel("");
        label.setBounds(x, y, 50, 50);
        return label;
    }

    private JLabel weatherIconLabel() {
        JLabel label = new JLabel();
        label.setBounds(15, 50, 75, 75);
        return label;
    }

    private JTextField cityTextField() {
        JTextField textField = new JTextField(10);
        textField.setBounds(65, 15, 150, 25);
        return textField;
    }

    private class MainWindowMenuBar extends JMenuBar {

        private JMenu getWeather;
        private JMenu config;
        private JMenu about;

        public MainWindowMenuBar() {
            this.getWeather = new JMenu("Weather");
            this.config = new JMenu("Configuration");
            this.about = new JMenu("Help");
            menuBarIni();
        }

        private void menuBarIni() {
            this.add(getWeather);
            this.add(config);
            this.add(about);
            addItemsToBar();
            aboutMenuIni();
        }

        private void addItemsToBar() {
            getWeather.add(getWeatherItem("get weather"));
            getWeather.addSeparator();
            getWeather.add(getWeatherItem("recent cities"));
            getWeather.addSeparator();
            getWeather.add(getWeatherItem("Exit"));
        }

        private void aboutMenuIni(){
            JMenuItem aboutItem = new JMenuItem("About");
            aboutItem.addActionListener(new AboutListener());
            about.add(aboutItem);
        }

        private JMenuItem getWeatherItem(String name) {
            JMenuItem item = new JMenuItem(name);
            item.setMnemonic(KeyEvent.VK_0);
            return item;
        }
    }
}
