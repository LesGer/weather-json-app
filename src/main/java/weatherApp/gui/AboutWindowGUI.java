package weatherApp.gui;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class AboutWindowGUI {

    private JFrame frame;

    public AboutWindowGUI(){
        frame = new JFrame();
        frameInit();
    }

    private void frameInit(){
        frame.setTitle("About");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setPreferredSize(new Dimension(500,500));
        frame.getContentPane().add(setAndInitPanel());
        frame.pack();
        frame.setVisible(true);
    }

    private JPanel setAndInitPanel(){
        JPanel panel = new JPanel();
        panel.setLayout(null);
        panel.add(licenceLabel());
        return panel;
    }


    private JLabel licenceLabel(){
        JLabel label = new JLabel();
        label.setBounds(15,15,470,470);
        try{
            label.setText(getLicenceFromFile());
        } catch (IOException e) {
            e.printStackTrace();
        }


        return label;
    }

    private String getLicenceFromFile() throws IOException{
        String pathName = "src" + File.separator + "main" + File.separator + "resources"
                + File.separator + "licence.txt";

        return Files.toString(new File(pathName), Charsets.UTF_8);

    }

}
