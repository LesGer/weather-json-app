package weatherApp.gui;

import javax.swing.*;
import java.awt.*;

public class GUIItems {

    private JFrame frame;
    private JLabel[] weatherInfo;
    private JTextField cityText;
    private JLabel weatherIcon;
    private JLabel[] weatherInfoIcons;
    private Dimension dimension;
    private boolean frameExpanded;

    public GUIItems(){
        this.frame = new JFrame();
        this.weatherIcon = new JLabel();
        this.cityText = new JTextField();
        this.weatherInfo = new JLabel[8];
        this.weatherInfoIcons = new JLabel[6];
        this.dimension = new Dimension(400, 220);
        this.frameExpanded = false;
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JLabel[] getWeatherInfo() {
        return weatherInfo;
    }

    public void setWeatherInfo(JLabel[] weatherInfo) {
        this.weatherInfo = weatherInfo;
    }

    public JTextField getCityText() {
        return cityText;
    }

    public void setCityText(JTextField cityText) {
        this.cityText = cityText;
    }

    public JLabel getWeatherIcon() {
        return weatherIcon;
    }

    public void setWeatherIcon(JLabel weatherIcon) {
        this.weatherIcon = weatherIcon;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public boolean isFrameExpanded() {
        return frameExpanded;
    }

    public void setFrameExpanded(boolean frameExpanded) {
        this.frameExpanded = frameExpanded;
    }

    public JLabel[] getWeatherInfoIcons() {
        return weatherInfoIcons;
    }

    public void setWeatherInfoIcons(JLabel[] weatherInfoIcons) {
        this.weatherInfoIcons = weatherInfoIcons;
    }
}
