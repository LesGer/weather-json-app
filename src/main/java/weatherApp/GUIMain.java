package weatherApp;


import weatherApp.gui.AboutWindowGUI;
import weatherApp.gui.KeyWindowGUI;
import weatherApp.gui.MainWindowGUI;

public class GUIMain {
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() { //fragment
            public void run() { // kodu odpowiedzialny za umieszczenie wywołania
                // w specjalnym dla Swinga wątku event dispatcher
                new KeyWindowGUI();
            }
        });
    }
}

