package weatherApp.logic;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import weatherApp.model.Country;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;

public class ReadCountriesFromServer {

    Map<String,String> countriesAndCapitals;

    public ReadCountriesFromServer() {
        try {
            this.countriesAndCapitals = createMapFromServer();
        } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
            e.printStackTrace();
        }
    }

    private Map<String,String> createMapFromServer() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        SetupUnirest.execute();
        Map<String,String> map = new LinkedHashMap<>();

        try {
            Country[] countries = Unirest.get("https://restcountries.eu/rest/v2/all").asObject(Country[].class).getBody();
            for(Country c: countries) {
                map.put(c.getName(),c.getCapital());
            }
            System.out.println("Załadowano informacje");
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return map;
    }

    public Map<String, String> getCountriesAndCapitals() {
        return countriesAndCapitals;
    }
}
