package weatherApp.logic.listeners;

import weatherApp.gui.GUIItems;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ExpandListener implements ActionListener {

    private GUIItems guiItems;


    public ExpandListener(GUIItems GUIItems) {
        this.guiItems = GUIItems;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!guiItems.isFrameExpanded()) {
            guiItems.getDimension().height = 450;
            guiItems.getFrame().setSize(guiItems.getDimension());
            guiItems.setFrameExpanded(true);
        } else {
            guiItems.getDimension().height = 220;
            guiItems.getFrame().setSize(guiItems.getDimension());
            guiItems.setFrameExpanded(false);
        }
    }
}
