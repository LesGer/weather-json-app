package weatherApp.logic.listeners;

import weatherApp.gui.GUIItems;
import weatherApp.model.Config;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.util.Map;

public class ListenerFactory {


    public static ActionListener factory(String text, Config config, GUIItems guiItems) {
        ActionListener listener = null;
        if ("get".equalsIgnoreCase(text)) {
            return new GetWeatherListener(config, guiItems);
        }
        if ("expand".equalsIgnoreCase(text)) {
            return new ExpandListener(guiItems);
        }
        return listener;
    }

    public static ActionListener factory(String command, JFrame frame, JPasswordField apiKey, Config config){
        ActionListener listener = null;

        if("ok".equalsIgnoreCase(command)){
            return new OpenMainWindowListener(frame,apiKey, config);
        }
        if("help".equalsIgnoreCase(command)){
            return new GetApiKeyListener();
        }

        return listener;
    }


    public static ItemListener factory(String capital, Config config, Map<String, String> countryMap, JTextField cityText) {
        ItemListener listener = null;
        if ("combo".equalsIgnoreCase(capital)) {
            return new ComboItemListener(countryMap, config, cityText);
        }
        return listener;
    }
}

