package weatherApp.logic.listeners;

import weatherApp.model.Config;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Map;

public class ComboItemListener implements ItemListener {

    private Map<String,String> countryMap;
    private Config config;
    private JTextField cityText;

    public ComboItemListener(Map<String,String> map, Config config, JTextField cityText){
        this.countryMap = map;
        this.config = config;
        this.cityText = cityText;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        config.setCity(countryMap.get(e.getItem().toString()));
        cityText.setText(config.getCity());
    }
}
