package weatherApp.logic.listeners;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class GetApiKeyListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        if(Desktop.isDesktopSupported()){
            try {
                Desktop.getDesktop().browse(new URI("http://openweathermap.org/appid"));
            } catch (IOException | URISyntaxException error) {
                error.printStackTrace();
            }
        }
    }
}
