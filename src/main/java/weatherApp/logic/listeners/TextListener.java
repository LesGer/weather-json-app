package weatherApp.logic.listeners;

import weatherApp.model.Config;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class TextListener implements DocumentListener {

    private Config config;
    private JTextField textField;

    public TextListener(Config config, JTextField textField) {
        this.config = config;
        this.textField = textField;
    }


    @Override
    public void insertUpdate(DocumentEvent e) {
        System.out.println("insert");
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        System.out.println("remove");
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        System.out.println("change");
    }
}
