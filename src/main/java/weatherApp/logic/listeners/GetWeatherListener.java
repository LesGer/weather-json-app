package weatherApp.logic.listeners;

import weatherApp.gui.GUIItems;
import weatherApp.logic.ReadWeatherFromServer;
import weatherApp.model.Config;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class GetWeatherListener implements ActionListener {

    private Config config;
    private GUIItems GUIItems;

    public GetWeatherListener(Config config, GUIItems GUIItems) {
        this.config = config;
        this.GUIItems = GUIItems;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        config.setCity(GUIItems.getCityText().getText());
        ReadWeatherFromServer read = new ReadWeatherFromServer(config);
        setLabelsFromServerData(read);
        setWeatherInfoIcon();

    }

    private void setLabelsFromServerData(ReadWeatherFromServer read) {
        GUIItems.getWeatherIcon().setIcon(weatherIcon(read.getWeather().getWeather()[0].getIcon()));

        GUIItems.getWeatherInfo()[0].setText(String.format("%d °C", read.getWeather().getMain().getTemp()));
        GUIItems.getWeatherInfo()[1].setText(read.getWeather().getWeather()[0].getDescription());
        GUIItems.getWeatherInfo()[2].setText(formatString("Max. temp", "°C", read.getWeather().getMain().getTempMax()));
        GUIItems.getWeatherInfo()[3].setText(formatString("Min. temp", "°C", read.getWeather().getMain().getTempMin()));
        GUIItems.getWeatherInfo()[4].setText(formatString("Pressure", "hPa", read.getWeather().getMain().getPressure()));
        GUIItems.getWeatherInfo()[5].setText(formatString("Humidity", "%", read.getWeather().getMain().getHumidity()));
        GUIItems.getWeatherInfo()[6].setText(windDirection(read.getWeather().getWind().getDeg()));
        GUIItems.getWeatherInfo()[7].setText(formatString("Wind speed", "m/s", read.getWeather().getWind().getSpeed()));
    }

    private String formatString(String name, String unit, int value) {
        StringBuilder builder = new StringBuilder();
        builder.append("<html>");
        builder.append(name);
        builder.append("<br/>");
        builder.append(value);
        builder.append(" ");
        builder.append(unit);
        builder.append("</html>");
        return builder.toString();
    }

    private void setWeatherInfoIcon() {
        String[] iconNames = {"max.png", "min.png", "barometer.png", "hum.png", "windDir.png", "windSpeed.png"};
        String pathName = "src" + File.separator + "main" + File.separator + "resources"
                + File.separator + "infoIcons" + File.separator;
        JLabel[] weatherLabels = GUIItems.getWeatherInfoIcons();

        for (int i = 0; i < iconNames.length; i++) {
            weatherLabels[i].setIcon(new ImageIcon(pathName + iconNames[i]));
        }

    }

    private ImageIcon weatherIcon(String iconID) {
        ImageIcon icon = new ImageIcon("src" + File.separator + "main" + File.separator + "resources"
                + File.separator + iconID + ".png");
        return icon;
    }

    protected String windDirection(int direction) {

        if (direction < 30) {
            return "North";
        } else if (direction < 75) {
            return "North-East";
        } else if (direction < 115) {
            return "East";
        } else if (direction < 150) {
            return "South-East";
        } else if (direction < 195) {
            return "South";
        } else if (direction < 240) {
            return "South-West";
        } else if (direction < 285) {
            return "West";
        } else if (direction < 330) {
            return "North-West";
        } else {
            return "North";
        }

    }

}
