package weatherApp.logic.listeners;

import weatherApp.gui.MainWindowGUI;
import weatherApp.model.Config;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OpenMainWindowListener implements ActionListener {

    private JFrame windowFrame;
    private JPasswordField apiKey;
    private Config config;

    public OpenMainWindowListener(JFrame frame, JPasswordField apiKey, Config config){
        this.windowFrame = frame;
        this.apiKey = apiKey;
        this.config = config;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(apiKey.getPassword().length != 0){
            config.setApiKey(String.copyValueOf(apiKey.getPassword()));
            windowFrame.dispose();
            new MainWindowGUI(config);
        } else {
            System.out.println("brak apikey");
        }




    }
}
