package weatherApp.logic.listeners;

import weatherApp.gui.AboutWindowGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AboutListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        new AboutWindowGUI();
    }
}
