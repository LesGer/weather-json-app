package weatherApp.logic;


import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import weatherApp.model.Config;
import weatherApp.model.Weather;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class ReadWeatherFromServer {

    private Weather weather;
    private Config config;

    public ReadWeatherFromServer(Config config) {
        this.config = config;
        try {
            this.weather = getWeatherFromServer();
        } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
            e.printStackTrace();
        }
    }

    private Weather getWeatherFromServer() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        SetupUnirest.execute();
        Weather weather = null;
        try {
//            "ed18cf64a1a6ac7c174d9ea1a02035a5"
            weather = Unirest.get("http://api.openweathermap.org/data/2.5/weather")
                    .queryString("q",config.getCity())
                    .queryString("APPID",config.getApiKey())
                    .queryString("units",config.getUnits())
                    .queryString("lang",config.getLang())
                    .asObject(Weather.class).getBody();

        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return weather;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }
}
