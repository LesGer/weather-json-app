package weatherApp.model;

public class Config {

    private String apiKey;
    private String units;
    private String lang;
    private String city;

    public Config() {
        this.apiKey = null;
        this.units = "metric";
        this.lang = "en";
        this.city = null;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
