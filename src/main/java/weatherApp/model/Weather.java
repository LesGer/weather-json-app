package weatherApp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import weatherApp.model.WeatherInfo;
import weatherApp.model.WeatherMain;
import weatherApp.model.WindInfo;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {

    private WeatherInfo[] weather;
    private WeatherMain main;
    private WindInfo wind;
    private String name;
    private int id;

    public Weather() {
    }

    public WeatherInfo[] getWeather() {
        return weather;
    }

    public void setWeather(WeatherInfo[] weather) {
        this.weather = weather;
    }

    public WeatherMain getMain() {
        return main;
    }

    public void setMain(WeatherMain main) {
        this.main = main;
    }

    public WindInfo getWind() {
        return wind;
    }

    public void setWind(WindInfo wind) {
        this.wind = wind;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "weather=" + weather +
                ", main=" + main +
                ", wind=" + wind +
                ", name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
