import com.mashape.unirest.http.Headers;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class UnirestExample {
    public static void main(String[] args) throws UnirestException {

        String body = Unirest.get("http://192.168.1.17:8080/hi").asString().getBody();

        System.out.println("odpowiedz z serwera /hi " + body );

        HttpResponse<String> stringHttpResponse = Unirest.get("http://192.168.1.17:8080/hi").asString();
        System.out.println("http response " + stringHttpResponse);
        int responseCode = stringHttpResponse.getStatus();
        Headers headers = stringHttpResponse.getHeaders();
        String responseBody = stringHttpResponse.getBody();

        System.out.println(headers);
        System.out.println(responseCode + " " + responseBody);
    }
}
